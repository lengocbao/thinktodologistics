import React from "react";
import ReactDOM from "react-dom";
import style from "./css/style.css";
export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = { status1: "active", status2: "", status3: "" };
        this.changeState1 = this.changeState1.bind(this);
        this.changeState2 = this.changeState2.bind(this);
        this.changeState3 = this.changeState3.bind(this);
    }   
    changeState1() {
        this.setState({ status1: "active", status2: "", status3: "" });
    }
    changeState2() {
        this.setState({ status1: "", status2: "active", status3: "" });
    }
    changeState3() {
        this.setState({ status1: "", status2: "", status3: "active" });
    }
    render() {
        return (
            <body class="all hold-transition skin-blue sidebar-mini">
                <div class="wrapper">

                    {/* <!-- Main Header --> */}
                    <div class="main-header">

                        {/* <!-- Logo --> */}
                        <a href="index2.html" class="logo">
                            {/* <!-- mini logo for sidebar mini 50x50 pixels --> */}
                            <span class="logo-mini"><b>A</b>LT</span>
                            {/* <!-- logo for regular state and mobile devices --> */}
                            <span class="logo-lg"><b>Admin</b>LTE</span>
                        </a>

                        {/* <!-- Header Navbar --> */}
                        <nav class="navbar navbar-static-top" role="navigation">
                            {/* <!-- Sidebar toggle button--> */}
                            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                                <span class="sr-only">Toggle navigation</span>
                            </a>

                        </nav>
                    </div>
                    {/* <!-- Left side column. contains the logo and sidebar --> */}
                    <aside class="main-sidebar">

                        {/* <!-- sidebar: style can be found in sidebar.less --> */}
                        <section class="sidebar">

                            {/* <!-- Sidebar user panel (optional) --> */}
                            <div class="user-panel">
                                <div class="pull-left image">
                                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                                </div>
                                <div class="pull-left info">
                                    <p>Alexander Pierce</p>
                                    {/* <!-- Status --> */}
                                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                                </div>
                            </div>

                            {/* <!-- search form (Optional) --> */}
                            <form action="#" method="get" class="sidebar-form">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="Search..." />
                                    <span class="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                            {/* <!-- /.search form --> */}

                            {/* <!-- Sidebar Menu --> */}
                            <ul class="sidebar-menu" data-widget="tree">
                                <li class="header">OPTOINS</li>
                                {/* <!-- Optionally, you can add icons to the links --> */}
                                <li class={this.state.status1} onClick={this.changeState1}><a href="#"><i class="fa fa-link"></i> <span>List users</span></a></li>
                                <li class={this.state.status2} onClick={this.changeState2}><a href="#"><i class="fa fa-link"></i> <span>Oders</span></a></li>
                                <li class={this.state.status3} onClick={this.changeState3}><a href="#"><i class="fa fa-link"></i> <span>Setting</span></a></li>
                            </ul>
                            {/* <!-- /.sidebar-menu --> */}
                        </section>
                        {/* <!-- /.sidebar --> */}
                    </aside>

                    {/* <!-- Content Wrapper. Contains page content --> */}
                    <div class="content-wrapper">
                        {/* <!-- Content Header (Page header) --> */}
                        <section class="content-header">
                            <h1>
                                List users
                            </h1>

                        </section>

                        {/* <!-- Main content --> */}
                        <section class="content container-fluid">

                            {/* <!--------------------------
        | Your Page Content Here |










        --------------------------> */}

                        </section>
                        {/* <!-- /.content --> */}
                    </div>
                    {/* <!-- /.content-wrapper --> */}

                    {/* <!-- Main Footer --> */}
                    <footer class="main-footer">
                        {/* <!-- To the right --> */}
                        <div class="pull-right hidden-xs">
                            Anything you want
                        </div>
                        {/* <!-- Default to the left --> */}
                        <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
                    </footer>


                </div>
            </body>
        )
    }
}