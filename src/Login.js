import React from "react";
import ReactDOM from "react-dom";
import style from "./css/style.css";
export default class Login extends React.Component {
    constructor(props) {
        super(props);
    }
  
    render() {
        return (
            <body class="hold-transition login-page">
                <div class="login-box">
                    <div class="login-logo">
                        <a href="../../index2.html"><b>Admin</b>LTE</a>
                    </div>
                    {/* <!-- /.login-logo --> */}
                    <div class="login-box-body">
                        <p class="login-box-msg">Sign in to start your session</p>
                        <form action="../../index2.html" method="post">
                            <div class="form-group has-feedback">
                                <input type="email" class="form-control" placeholder="Email" />
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" class="form-control" placeholder="Password" />
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="d-flex justify-content-between">
                                
                                    <div class="">
                                        
                                            <input type="checkbox" /> Remember Me
                                        
                                    </div>
                                
                                {/* <!-- /.col --> */}
                                
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                                
                                {/* <!-- /.col --> */}
                            </div>
                        </form>              
                        <a href="#">I forgot my password</a><br></br>
                        <a href="register.html" class="text-center">Register a new membership</a>
                    </div>
                </div>
            </body>
        )
    }
}